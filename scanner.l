%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "support.h"
#include "y.tab.h"

#ifndef YYSTYPE
#define YYSTYPE Node*
#endif

extern YYSTYPE yylval;

char* str_clone(const char* str) {
    char* clone = static_cast<char*>(malloc(sizeof(char) * (1 + strlen(str))));
    strcpy(clone, str);
    return clone;
}
%}

%x COMMENT

%%

"/*" {
    BEGIN COMMENT;
}

<COMMENT>"*/" {
    BEGIN INITIAL;
}

"//".* {
}

(void|int|double|float|char) {
    yylval = new Node(yytext);
    return TYPE;
}

"const" {
    return CONST;
}

"if" {
    return IF;
}

"else" {
    return ELSE;
}

"switch" {
    return SWT;
}

"case" {
    return CASE;
}

"default" {
    return DFT;
}

"do" {
    return DO;
}

"while" {
    return WHL;
}

"for" {
    return FOR;
}

"return" {
    return RET;
}

"break" {
    return BRK;
}

"continue" {
    return CONT;
}

(NULL|struct) {
}

<COMMENT>(void|const|NULL|for|do|while|break|continue|if|else|return|struct|switch|case|default|int|double|float|char) {
}

<COMMENT>(fclose|clearerr|feof|ferror|fflush|fgetpos|fopen|fread|freopen|fseek|fsetpos|ftell|fwrite|remove|rename|rewind|setbuf|setvbuf|tmpfile|tmpnam|fprintf|printf|sprintf|vfprintf|vprintf|vsprintf|fscanf|scanf|sscanf|fgetc|fgets|fputc|fputs|getc|getchar|gets|putc|putchar|puts|ungetc|perror) {
}

(\t|" ") {
}

<COMMENT>(\t|" ") {
}

\n  {
}

<COMMENT>\n {
}

[_a-zA-Z][_a-zA-Z0-9]{0,30} {
    yylval = new Node(yytext);
    return IDENT;
}

<COMMENT>[_a-zA-Z][_a-zA-Z0-9]{0,30} {
}

(\+|-|\*|\/|%|<|>|=|!|&|\|) {
    return yytext[0];
}

"++" {
    return INC;
}

"--" {
    return DEC;
}

"<=" {
    return LEQ;
}

">=" {
    return GEQ;
}

"&&" {
    return LAND;
}

"||" {
    return LOR;
}

"==" {
    return EQ;
}

"!=" {
    return NEQ;
}

<COMMENT>(\+|-|\*|\/|%|\+\+|--|<|<=|>|>=|==|!=|=|&&|\|\||!|&|\|) {
}

(:|;|,|\.|\[|\]|\(|\)|\{|\}) {
    return yytext[0];
}

<COMMENT>(:|;|,|\.|\[|\]|\(|\)|\{|\}) {
}

[0-9]+ {
    int val = atoi(yytext);
    static char str[1000];
    sprintf(str, "%d", val);

    yylval = new Node(str);
    return INTLIT;
}

<COMMENT>[+-]?[0-9][0-9]* {
}

[0-9]*(\.|\.[0-9]+)? {
    float val = atof(yytext);
    static char str[1000];
    sprintf(str, "%f", val);

    yylval = new Node(str);
    return FLTLIT;
}

<COMMENT>[+-]?[0-9]*(\.|\.[0-9]+)? {
}

["]([^"\\\n]|\\.|\\\n)*["] {
    yylval = new Node(yytext);
    return STRLIT;
}

<COMMENT>["]([^"\\\n]|\\.|\\\n)*["] {
}

"'\t'" {
    yylval = node_new_clone_str("\t");
    return CHARLIT;
}

"'\n'" {
    yylval = node_new_clone_str("\n");
    return CHARLIT;
}

'\\'' {
    yylval = node_new_clone_str("'");
    return CHARLIT;
}

'\\\\' {
    yylval = node_new_clone_str("\\");
    return CHARLIT;
}

'.' {
    yylval = node_new_clone_ch(yytext[1]);
    return CHARLIT;
}

<COMMENT>('.'|'\t'|'\n'|'\0') {
}

. {
    fprintf(stderr, "\033[31mUNMATCHED: \"%s\"\033[0m", yytext);
    exit(1);
}

<COMMENT>. {
}

%%

int yywrap(void) {
    return 1;
}
